package com.cy.jt;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@SpringBootTest
public class JWTTest {
    private String secret = "woshinibaba";

    @Test
    public void testCreateAndParseToken() throws InterruptedException {
        //1.创建令牌
        //1.1定义负载信息
       Map<String,Object> map = new HashMap<>();
       map.put("username", "admin");
       map.put("permission", "sys:res:retrieve,sys:res:create");


        //1.2定义过期实践
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MINUTE, 30);
        Date expirationTime = calendar.getTime();


        //1.4生成令牌
        String token = Jwts.builder()
                .setClaims(map)
                .setExpiration(expirationTime)
                .setIssuedAt(new Date())
                .signWith(SignatureAlgorithm.HS256, secret)
                .compact();
        System.out.println("token:" + token);


        //2.解析令牌
        Claims claims = Jwts.parser().setSigningKey(secret)
                .parseClaimsJws(token)
                .getBody();
        System.out.println("claims :" + claims);


    }
}
