package com.cy.jt.interceptor;

import com.cy.jt.util.JwtUtils;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
/**
 * 令牌(token:ticker-通票)拦截器
 * 其中,HandlerInterceptor为Spring MVC中的拦截器,
 * 可以在Controller方法执行之前之后执行一些动作.
 * 1)Handler 处理器(Spring MVC中将@RestController描述的类看成是处理器)
 * 2)Interceptor 拦截器
 */
public class TokenInterceptor implements HandlerInterceptor {
    /**
     * preHandle在目标Controller方法执行之前执行
     * @param handler 目标Controller对象
     */
    @Override
    public boolean preHandle(HttpServletRequest request,
                             HttpServletResponse response,
                             Object handler) throws Exception {

        System.out.println("==preHandle==");
        String token = request.getHeader("Authentication");
        if (token==null || "".equals(token)){
            throw new RuntimeException("您还没有登录,请先登录");
        }
        Boolean tokenExpired = JwtUtils.isTokenExpired(token);
        if (tokenExpired){
            throw new RuntimeException("登录超时,请重新登录");
        }
        return true;

    }
}
