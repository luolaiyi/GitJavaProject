package com.cy.jt.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ResourceController {

    @RequestMapping("/update")
    public String doUpdate() {
        //检查用户有没有登录
        //执行业务查询操作,但在执行此操作之前要检查用户有没有登录
        return "执行更新操作";
    }

    @RequestMapping("/retrieve")
    public String doRetrieve() {
        //检查用户有没有登录
        //执行业务查询操作,但在执行此操作之前要检查用户有没有登录
        return "执行查询操作";
    }
}
