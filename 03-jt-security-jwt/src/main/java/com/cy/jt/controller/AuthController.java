package com.cy.jt.controller;

import com.cy.jt.util.JwtUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
public class AuthController {
    @RequestMapping("/login")
    public Map<String,Object> doLogin(String username,String password){
        Map<String,Object> map = new HashMap<>();

        if ("admin".equals(username) && "admin".equals(password)){
            map.put("state", "200");
            map.put("message","登陆成功");
            Map<String,Object> claims = new HashMap<>();
            claims.put("username",username);
            map.put("Authentication", JwtUtils.generateToken(claims));
            return map;
        }else {
            map.put("state", "500");
            map.put("message","登录失败");
            return map;
        }
    }
}
