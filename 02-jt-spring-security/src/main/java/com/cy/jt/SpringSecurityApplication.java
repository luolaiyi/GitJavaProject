package com.cy.jt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@SpringBootApplication
public class SpringSecurityApplication {
    public static void main(String[] args) {
        SpringApplication.run(SpringSecurityApplication.class, args);
        //encodePwd();
    }

//    public static void encodePwd(){
//        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
//        String newPassword = encoder.encode("admin");
//        //System.out.println(newPassword);
//    }
}
