package com.cy.jt.config;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class RedirectAuthenticationSuccessHandler implements AuthenticationSuccessHandler {
    private String redirectURL;

    public RedirectAuthenticationSuccessHandler(String redirectURL) {
        this.redirectURL = redirectURL;
    }

    @Override
    public void onAuthenticationSuccess(HttpServletRequest httpServletRequest,
                                        HttpServletResponse httpServletResponse,
                                        Authentication authentication)
            throws IOException, ServletException {
         httpServletResponse.sendRedirect(redirectURL);
    }
}
