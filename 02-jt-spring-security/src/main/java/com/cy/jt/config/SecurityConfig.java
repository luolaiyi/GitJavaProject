package com.cy.jt.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.server.authentication.RedirectServerAuthenticationFailureHandler;
import org.springframework.security.web.server.authentication.RedirectServerAuthenticationSuccessHandler;

//这个配置类是配置Spring-Security的,
//prePostEnabled= true表示启动权限管理功能
@EnableGlobalMethodSecurity(prePostEnabled = true)
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable();//关闭跨域攻击，不关闭容易出错
        http.formLogin()//自定义登陆表单
                .loginPage("/login.html").//设置登陆页面
                loginProcessingUrl("/login").//设置登陆请求处理地址(对应form表单中的action),登陆时会访问UserDetailService对象
                defaultSuccessUrl("/index.html");//设置登陆成功跳转页面(默认为/index.html)
                //successHandler(new RedirectAuthenticationSuccessHandler("https://www.csdn.net/"));
                // successHandler(new JsonRedirectAuthenticationSuccessHandler()).
                //failureHandler(new JsonRedirectAuthenticationFailureHandler());
        http.logout().logoutUrl("/logout").logoutSuccessUrl("/login.html?logout");
        http.exceptionHandling().accessDeniedHandler(new
                                              DefaultAccessDeniedExceptionHandler());
        http.exceptionHandling().authenticationEntryPoint(new
                DefaultAuthenticationEntryPoint());
        http.authorizeRequests() //设置请求的授权
                .antMatchers(   //配置下列路径的授权
                        "/index.html",
                        "/js/*",
                        "/css/*",
                        "/img/**",
                        "/bower_components/**",
                        "/login.html",
                        "/images/**"
                ).permitAll().anyRequest().authenticated();//除了以上资源必须认证才可访问;   //设置上述所有路径不需要登录就能访问(放行)
//设置需要认证的请求（除了上面的要放行，其它都要进行认证）
    }

    @Bean
    public BCryptPasswordEncoder encoderPassword() {
        return new BCryptPasswordEncoder();
    }
}
