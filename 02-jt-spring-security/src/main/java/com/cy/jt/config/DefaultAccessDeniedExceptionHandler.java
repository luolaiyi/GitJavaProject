package com.cy.jt.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

public class DefaultAccessDeniedExceptionHandler implements AccessDeniedHandler {
    @Override
    public void handle(HttpServletRequest httpServletRequest,
                       HttpServletResponse httpServletResponse,
                       AccessDeniedException e) throws IOException, ServletException {
       httpServletResponse.setCharacterEncoding("utf-8");
       httpServletResponse.setContentType("application/json;charset=utf-8");
       //httpServletResponse.sendRedirect("https://www.baidu.com");
        PrintWriter writer = httpServletResponse.getWriter();
        Map<String,Object> map = new HashMap<>();
        map.put("state", httpServletResponse.SC_FORBIDDEN);//403
        map.put("message","没有访问权限，请联系管理员");
        String string = new ObjectMapper().writeValueAsString(map);//JSON格式转换
        writer.print(string);
        writer.flush();
    }
}
