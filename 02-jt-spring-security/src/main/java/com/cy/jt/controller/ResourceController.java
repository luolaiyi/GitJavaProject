package com.cy.jt.controller;


import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ResourceController {
    @PreAuthorize("hasRole('admin')")
    @RequestMapping("/doCreate")
    public String doCreate() {
        return "允许执行添加操作";
    }

    @PreAuthorize("hasRole('admin')")
    @RequestMapping("/doRetrieve")
    public String doRetrieve() {
        return "允许执行查询操作";
    }

    @PreAuthorize("hasRole('other')")
    @RequestMapping("/doUpdate")
    public String doUpdate() {
        return "允许执行更新操作";
    }

    @PreAuthorize("hasAuthority('sys:res:delete')")
    @RequestMapping("/doDelete")
    public String doDelete() {
        return "允许执行删除操作";
    }

    /**
     * 获取登录用户信息?
     * 1)用户登录成功以后信息存储到了哪里
     * 2)如何获取用户登录信息
     * @return
     */
    @GetMapping("/doGetUser")
    public String doGetUser(){//80B0FEDCFF54E425B08C74B2E100D69E
        //从Session中获取用户认证信息
        //1)Authentication 认证对象(封装了登录用户信息的对象)
        //2)SecurityContextHolder 持有登录状态的信息的对象(底层可通过session获取用户信息)
        Authentication authentication =
                SecurityContextHolder.getContext().getAuthentication();
        //基于认证对象获取用户身份信息
        User principal = (User)authentication.getPrincipal();
        System.out.println("principal.class="+principal.getClass());
        return principal.getUsername()+" : "+principal.getAuthorities();
    }
}
