package com.cy.jt;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@SpringBootTest
public class BCryptTest {
    @Test
    public void encodePwd(){
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        String newPassword = encoder.encode("admin");
        System.out.println(newPassword);
        System.out.println(newPassword.length());
        boolean admin = encoder.matches("admin", newPassword);
        System.out.println("flag:" + admin);
    }
}
