package com.cy.feign;

import com.cy.service.RemoteProviderService;
import feign.hystrix.FallbackFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class HystrixFeignFallBackFactory implements FallbackFactory<RemoteProviderService> {

    private static final Logger LOGGER = LoggerFactory.getLogger(HystrixFeignFallBackFactory.class);

    @Override
    public RemoteProviderService create(Throwable throwable) {
       return new RemoteProviderService() {
           @Override
           public String echoMsg(String msg) {
               return "沙雕，出BUG了吧" + throwable.getMessage();
           }
       };

    }
}
