package com.cy;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.concurrent.atomic.AtomicLong;

@EnableFeignClients
@SpringBootApplication
public class ConsumerApplication {
    public static void main(String[] args) {
        SpringApplication.run(ConsumerApplication.class, args);
    }

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @Bean
    @LoadBalanced
    public RestTemplate LoadBalancedRestTemplate(){
        return new RestTemplate();
    }

    @RestController
    public class ConsumerController {
        @Autowired
        private RestTemplate restTemplate;
        @Autowired
        private RestTemplate LoadBalancedRestTemplate;
        @Autowired
        private LoadBalancerClient loadBalancerClient;

        @Value("${server.port}")
        private String server;

        private AtomicLong atomicLong=new AtomicLong(1);

        @RequestMapping("/consumer/doRestEcho1")
        public String doRestEcho1() throws InterruptedException {
            long increment = atomicLong.getAndIncrement();
            if (increment%2==0){
                Thread.sleep(200);
            }
            String url = "http://localhost:8081/provider/echo/" + server;
            return restTemplate.getForObject(url, String.class);
        }

        @RequestMapping("/consumer/doRestEcho2")
        public String doRestEcho2(){
            ServiceInstance choose = loadBalancerClient.choose("sca-provider");
            String hostIP = choose.getHost();
            int port = choose.getPort();
            String url = "http://" + hostIP + ":" + port +"/provider/echo/" + server;
            return restTemplate.getForObject(url, String.class);
        }

        @RequestMapping("/consumer/doRestEcho3")
        public String doRestEcho3(){

            String url = String.format("http://%s/provider/echo/%s", "sca-provider",server);
            return LoadBalancedRestTemplate.getForObject(url, String.class);
        }
        @GetMapping("consumer/doFindById/{id}")
        @SentinelResource("FindById")
        public String doFindById(@PathVariable Integer id){
            return "resource id is "+ id ;
        }
    }
}
