package com.cy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
public class ProviderApplication {
    private static final Logger log = LoggerFactory.getLogger(ProviderApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(ProviderApplication.class, args);
    }




    @RefreshScope
    @RestController
    public class ProviderController {


        @Value("${server.port}")
        private String server;
        @Value("${logging.level.com.cy:error}")
        private String logLevel;
        @Value("${server.tomcat.threads.max:20}")
        private Integer maxThread;
        @Value("${page.pageSize:3}")
        private Integer  pageSize;

        @RequestMapping("/provider/doGetMaxThread")
        public String doGetMaxThread(){
            return "server.threads.max is  "+maxThread;
        }

        @GetMapping("/provider/echo/{msg}")
        public String doEcho(@PathVariable String msg) {
            if (msg == null || msg.length() < 3)
                throw new IllegalArgumentException();
            return server + " say:Hello Nacos Discovery " + msg;
        }
        @GetMapping("/provider/getPageSize")
        public String getPageSize(){
            return "page is " + pageSize;
        }

        @GetMapping("/provider/doGetLogLevel")
        public String doGetLogLevel() {
            log.trace("trace");
            log.debug("debug");
            log.info("info");
            log.warn("warn");
            log.error("error");
            return "log level is " + logLevel;

        }
    }

}
