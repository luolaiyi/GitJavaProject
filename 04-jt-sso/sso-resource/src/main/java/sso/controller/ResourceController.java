package sso.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ResourceController {
    @PreAuthorize("hasAuthority('sys:res:update')")
    @RequestMapping("/doUpdate")
    public String doUpdate() {
        //检查用户有没有登录
        //执行业务查询操作,但在执行此操作之前要检查用户有没有登录
        return "执行更新操作";
    }

    @PreAuthorize("hasAuthority('sys:res:retrieve')")
    @RequestMapping("/doRetrieve")
    public String doRetrieve() {
        //检查用户有没有登录
        //执行业务查询操作,但在执行此操作之前要检查用户有没有登录
        return "执行查询操作";
    }

    @PreAuthorize("hasAuthority('sys:res:create')")
    @RequestMapping("/doCreate")
    public String doCreate() {
        //检查用户有没有登录
        //执行业务查询操作,但在执行此操作之前要检查用户有没有登录
        return "执行新增操作";
    }

    @PreAuthorize("hasAuthority('sys:res:delete')")
    @RequestMapping("/deDelete")
    public String deDelete() {
        //检查用户有没有登录
        //执行业务查询操作,但在执行此操作之前要检查用户有没有登录
        return "执行删除操作";
    }
}
