package sso.util;

import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

public class WebUtils {
    public static void writeJsonToClient(HttpServletResponse response,
                                         Map<String, Object> map) throws IOException {
        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json;charset=utf-8");
        String JsonString = new ObjectMapper().writeValueAsString(map);
        PrintWriter printWriter = response.getWriter();
        printWriter.print(JsonString);
        printWriter.flush();
    }
}
