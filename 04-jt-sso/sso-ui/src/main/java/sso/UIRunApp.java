package sso;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UIRunApp {
    public static void main(String[] args) {
        SpringApplication.run(UIRunApp.class, args);
    }
}
