package sso.dao;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
/**SpringBootTest描述的类,一定要定义在启动类所在包或子包中*/
@SpringBootTest
public class DataSourceTests {
    @Autowired //javax.sql.DataSource;
    private DataSource dataSource;//数据源对象,获取连接的一个入口对象
    @Test //org.junit.jupiter.api.Test;
    void testGetConnection()throws SQLException {
        Connection conn=
             dataSource.getConnection();
        System.out.println(conn);
    }
}
