package com.cy.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

@FeignClient(name = "jt-sso-auth",contextId = "remoteAuthService")
public interface RemoteAuthService {
    @GetMapping("/auth/info")
    public Map<String,Object> getAuthentication(@RequestParam("token") String token);
}
