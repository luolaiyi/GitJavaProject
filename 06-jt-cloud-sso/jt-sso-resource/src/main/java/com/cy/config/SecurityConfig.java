package com.cy.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.access.AccessDeniedHandler;
import sso.util.WebUtils;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable();
        http.exceptionHandling().accessDeniedHandler(accessDeniedHandler());
        http.authorizeRequests().anyRequest().permitAll();
    }

    public AccessDeniedHandler accessDeniedHandler(){
        return (request,response,exception) -> {
            Map<String,Object> map = new HashMap<>();
            map.put("state", "403");
            map.put("message", "您没有权限访问服务器，请联系管理员");
            WebUtils.writeJsonToClient(response, map);
        };
    }
}
