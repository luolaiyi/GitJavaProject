package com.cy.controller;

import com.cy.utils.JwtUtils;
import io.jsonwebtoken.Claims;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class AuthenticationController {
    @GetMapping("/auth/info")
    public Map<String,Object> getAuthentication(String token){
        Claims claims = JwtUtils.getClaimsFromToken(token);
        Boolean expired = claims.getExpiration().before(new Date());
        String username = (String) claims.get("username");
        List<String> list=(List<String>)
                claims.get("authorities");
        Map<String,Object> map=new HashMap<>();
        map.put("expired",expired);
        map.put("username",username);
        map.put("authorities",list);
        return map;
    }
}
